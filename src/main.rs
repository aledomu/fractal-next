use gettextrs::*;

mod application;
#[rustfmt::skip]
mod config;
mod window;

use gtk::gio;

use application::ExampleApplication;
use config::{GETTEXT_PACKAGE, LOCALEDIR, PKGDATADIR};

fn main() {
    // Initialize logger, debug is carried out via debug!, info!, and warn!.
    pretty_env_logger::init();

    // Prepare i18n
    setlocale(LocaleCategory::LcAll, "");
    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
    textdomain(GETTEXT_PACKAGE);

    gtk::glib::set_application_name("Fractal");
    gtk::glib::set_prgname(Some("fractal"));

    gtk::init().expect("Unable to start GTK4");

    let res = gio::Resource::load(PKGDATADIR.to_owned() + "/resources.gresource")
        .expect("Could not load gresource file");
    gio::resources_register(&res);

    let app = ExampleApplication::new();
    app.run();
}
